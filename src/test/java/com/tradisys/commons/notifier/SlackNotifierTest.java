package com.tradisys.commons.notifier;

import org.junit.Ignore;
import org.junit.Test;

public class SlackNotifierTest {

    @Test
    @Ignore
    public void testMessage() throws Exception {
        Notifier notifier = new SlackNotifier("https://hooks.slack.com/services/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");
        notifier.warn("DApp scanning failed");
    }
}