package com.tradisys.commons.notifier;

import java.io.IOException;

public interface Notifier {
    void warn(String message) throws IOException;
}
