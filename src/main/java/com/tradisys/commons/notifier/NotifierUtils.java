package com.tradisys.commons.notifier;

import java.net.InetAddress;

public class NotifierUtils {

    public static String getLocalHostIp() {
        String serverNum = "#Unknown";
        try {
            InetAddress ip = InetAddress.getLocalHost();
            String address = ip.getHostAddress();
            serverNum = "#" + address.split("\\.")[3];
        } catch (Exception e) {
            // do nothing
        }
        return serverNum;
    }
}
