package com.tradisys.commons.notifier;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class SlackNotifier implements Notifier {

    private URL webHookUrl;
    private String env;

    public SlackNotifier(String webHookUrl) throws MalformedURLException {
        this(null, webHookUrl);
    }

    public SlackNotifier(String env, String webHookUrl) throws MalformedURLException {
        this.webHookUrl = new URL(webHookUrl);
        this.env = env;
    }

    @Override
    public void warn(String message) throws IOException {
        HttpsURLConnection con = (HttpsURLConnection) webHookUrl.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "Tradisys-Notifier");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");

        // Ensure the Connection Will Be Used to Send Content
        con.setDoOutput(true);

        try (BufferedOutputStream wr = new BufferedOutputStream(con.getOutputStream())) {
            String slackMsg = String.format("{" +
                    "\"text\": \"%1$s. Server %2$s\",\n" +
                    "\"blocks\": [\n" +
                    "{\n" +
                    "\"type\": \"context\",\n" +
                    "\"elements\": [\n" +
                    "{\n" +
                    "\"type\": \"image\",\n" +
                    "\"image_url\": \"https://api.slack.com/img/blocks/bkb_template_images/notificationsWarningIcon.png\",\n" +
                    "\"alt_text\": \"WARN\"\n" +
                    "},\n" +
                    "{\n" +
                    "\"type\": \"mrkdwn\",\n" +
                    "\"text\": \"%1$s. Server %2$s\"\n" +
                    "}\n" +
                    "]\n" +
                    "},\n" +
                    "{\n" +
                    "\"type\": \"divider\"\n" +
                    "}]}", message, env != null ? env : "Not Specified");
            wr.write(slackMsg.getBytes());
            wr.flush();
        }

        con.getResponseCode();

        /*BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());*/
    }
}
